import fileManipulation.FileManipulationController;
import logger.Logger;
import ui.TUIMain;

public class Main {
    private static FileManipulationController filesController = new FileManipulationController();

    public static void main(String[] args) {
        int userChoice;
        do {
            userChoice = TUIMain.showMainMenuAndGetUserSelection();
            switch (userChoice) {
                case 1:
                    listFilesInResourceFolder();
                    break;
                case 2:
                    selectExtensionAndShowFiles();
                    break;
                case 3:
                    getInformationAboutAFile();
                    break;

                case 9:
                    TUIMain.exitProgram();
                    break;
            }
        } while (userChoice != -1 && userChoice != 9);

    }

    private static void listFilesInResourceFolder() {
        long timeBeforeInvocation = System.currentTimeMillis();
        String[] fileNamesInDirectory = filesController.getFileNamesInFolder();
        long timeAfterInvocation = System.currentTimeMillis();
        long elapsedTime = timeAfterInvocation - timeBeforeInvocation;
        System.out.println("The files in the resource folder are:");
        Logger.writeToLog("Showing files in the resource folder.", elapsedTime);
        TUIMain.printArray(fileNamesInDirectory);
    }

    private static void selectExtensionAndShowFiles() {
        String[] fileExtensions = filesController.getExistingFileExtensionsInFolder();
        if (fileExtensions.length < 1) {
            System.out.println("There are no files in this folder...");
        } else {
            int extensionNumber = TUIMain.showAndSelectFileExtension(fileExtensions);
            if (extensionNumber == 9) {
                return;
            }
            String extensionType = fileExtensions[extensionNumber];
            long timeBeforeInvocation = System.currentTimeMillis();
            String[] filesWithSpecifiedExtension = filesController.getFilesWithExtension(extensionType);
            long timeAfterInvocation = System.currentTimeMillis();
            long elapsedTime = timeAfterInvocation - timeBeforeInvocation;
            Logger.writeToLog("Showing files with the file extension \"" + extensionType + "\".", elapsedTime);
            TUIMain.printArray(filesWithSpecifiedExtension);
        }
    }

    /**
     * Shows the menu for getting info about a specific text folder
     */
    private static void getInformationAboutAFile() {
        String[] textFiles = filesController.getFilesWithExtension(".txt");
        int chosenTextFile = TUIMain.showAndSelectFiles(textFiles);
        String chosenTextFileName = textFiles[chosenTextFile];

        int actionSelection = 0;
        do {
            actionSelection = TUIMain.showAndSelectTextFileMenu(chosenTextFileName);
            switch (actionSelection) {
                case 1:
                    getFileNameByFile(chosenTextFileName);
                    break;
                case 2:
                    getFileSize(chosenTextFileName);
                    break;
                case 3:
                    countNumberOfLinesInFile(chosenTextFileName);
                    break;
                case 4:
                    checkIfWordIsInFile(chosenTextFileName);
                    break;
                case 5:
                    countOccurencesOfWord(chosenTextFileName);
                    break;
            }
        } while (actionSelection != 9);
    }

    private static void getFileNameByFile(String chosenTextFileName) {
        long timeBeforeInvocation = System.currentTimeMillis();
        String fileName = filesController.getFileNameOfFileBy(chosenTextFileName);
        long timeAfterInvocation = System.currentTimeMillis();
        long elapsedTime = timeAfterInvocation - timeBeforeInvocation;
        logAndPrintToConsole("The name of " + chosenTextFileName + " is: \"" + fileName + "\"", elapsedTime);
    }

    private static void getFileSize(String chosenTextFileName) {
        long timeBeforeInvocation = System.currentTimeMillis();
        long fileSize = filesController.getFileSizeOfFile(chosenTextFileName);
        long timeAfterInvocation = System.currentTimeMillis();
        long elapsedTime = timeAfterInvocation - timeBeforeInvocation;
        if (fileSize < 1) {
            logAndPrintToConsole(
                    "The file \"" + chosenTextFileName + "\" seem to have dissappeared since you selected it...",
                    elapsedTime);
            return;
        }
        TUIMain.printFileSize(chosenTextFileName, fileSize, elapsedTime);
    }

    private static void countNumberOfLinesInFile(String chosenTextFileName) {
        long timeBeforeInvocation = System.currentTimeMillis();
        long numberOfLines = filesController.getNumberOfLinesInFile(chosenTextFileName);
        long timeAfterInvocation = System.currentTimeMillis();
        if (numberOfLines == 0) {
            logAndPrintToConsole("There was an error in the file or it has disappeared since you chose it.", 0);
        } else {
            Logger.writeToLog(chosenTextFileName + " has " + numberOfLines + " lines.",
                    timeAfterInvocation - timeBeforeInvocation);
            TUIMain.printNumberOfLines(chosenTextFileName, numberOfLines);
        }
    }

    private static void checkIfWordIsInFile(String chosenTextFileName) {
        String wordToLookFor = TUIMain.getWordSelectionFromUser();

        long timeBeforeInvocation = System.currentTimeMillis();
        boolean wordIsFound = filesController.isWordPresentInFile(chosenTextFileName, wordToLookFor);
        long timeAfterInvocation = System.currentTimeMillis();

        if (wordIsFound) {
            logAndPrintToConsole("\"" + wordToLookFor + "\" is in " + chosenTextFileName + ".",
                    timeAfterInvocation - timeBeforeInvocation);
        } else {
            logAndPrintToConsole("\"" + wordToLookFor + "\" is in " + chosenTextFileName + ".",
                    timeAfterInvocation - timeBeforeInvocation);
        }
    }

    private static void countOccurencesOfWord(String chosenTextFileName) {
        String wordToCount = TUIMain.getWordSelectionFromUser();

        long timeBeforeInvocation = System.currentTimeMillis();
        long occurrencesOfWord = filesController.occurrencesOfWordInFile(chosenTextFileName, wordToCount);
        long timeAfterInvocation = System.currentTimeMillis();

        logAndPrintToConsole(
                "\"" + wordToCount + "\" occurs " + occurrencesOfWord + " times in " + chosenTextFileName + ".",
                timeAfterInvocation - timeBeforeInvocation);
    }

    public static void logAndPrintToConsole(String message, long elapsedTime) {
        Logger.writeToLog(message, elapsedTime);
        System.out.println(message);
    }
}
