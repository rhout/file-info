package logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Writes information messages to the log
 */
public class Logger {
    public static void writeToLog(String message, long elapsedTime) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("program_log.log", true))) {
            String timeStamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
            bufferedWriter.append(timeStamp + ": " + message + " The function took " + elapsedTime + " ms.");
            bufferedWriter.newLine();
        } catch (IOException e) {
            System.out.println("There was an error with the file. Check output for details.");
            e.printStackTrace();
        }
    }
}
