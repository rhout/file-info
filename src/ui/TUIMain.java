package ui;

/**
 * Handles all the textual user interface action
 */
public class TUIMain {
    private static final int MAIN_MENU_ITEMS = 3;
    private static final int FILE_INFO_MENU_ITEMS = 5;

    /**
     * Show the main menu and let the user choose from one of them
     *
     * @return What menu point the user chose
     */
    public static int showMainMenuAndGetUserSelection() {
        ConsolePrinter.showMainMenu();
        return InputHandler.getIntFromUser(MAIN_MENU_ITEMS);
    }

    /**
     * Show available file extensions and let the user choose from one of them.
     * Subtracts one from the decision to represent the index array.
     *
     * @param availableFileExtensions
     * @return The user selection that corresponds to the position in the given
     *         array
     */
    public static int showAndSelectFileExtension(String[] availableFileExtensions) {
        ConsolePrinter.showFileExtensionsMenu(availableFileExtensions);
        int userChoice = InputHandler.getIntFromUser(availableFileExtensions.length);
        if (userChoice == 9) {
            return userChoice;
        }
        return userChoice - 1;
    }

    /**
     * Show available files and let the user choose from one of them. Subtracts one
     * from the decision to represent the index array.
     *
     * @param availableFiles
     * @return The user selection that corresponds to the position in the given
     *         array
     */
    public static int showAndSelectFiles(String[] availableFiles) {
        ConsolePrinter.printArrayNumbered(availableFiles);
        return InputHandler.getIntFromUser(availableFiles.length) - 1;
    }

    /**
     * Shows the menu for showing info for a specific file and makes the user select
     * an action.
     *
     * @param fileName
     * @return The selected number
     */
    public static int showAndSelectTextFileMenu(String fileName) {
        ConsolePrinter.showFileInfoMenu(fileName);
        return InputHandler.getIntFromUser(FILE_INFO_MENU_ITEMS);
    }

    /**
     * Print the size of a given file.
     *
     * @param fileName
     * @param fileSize
     */
    public static void printFileSize(String fileName, long fileSize, long elapsedTime) {
        ConsolePrinter.printFileSize(fileName, fileSize, elapsedTime);
    }

    /**
     * Print the line numbers of a given file.
     *
     * @param fileName
     * @param numberOfLines
     */
    public static void printNumberOfLines(String fileName, long fileNumber) {
        ConsolePrinter.printNumberOfLines(fileName, fileNumber);
    }

    /**
     * Prompts the user for a word.
     * @return
     */
    public static String getWordSelectionFromUser() {
        return InputHandler.getWordFromUser();
    }

    /**
     * Prints the strings from the given array
     *
     * @param array
     */
    public static void printArray(String[] array) {
        ConsolePrinter.printArray(array);
    }

    /** Handles when the user shuts down the program */
    public static void exitProgram() {
        ConsolePrinter.printExitMessage();
    }

}
