package ui;

import logger.Logger;

/**
 * Handles the output to the TUI
 */
public class ConsolePrinter {
    /**
     * Prints the main mennu to the console
     */
    public static void showMainMenu() {
        System.out.println();
        System.out.println("Welcome to the file reader system. What can I do for you?\n");
        System.out.println("1. Show the names of all the files in the resource folder");
        System.out.println("2. Show all files of a certain extension type");
        System.out.println("3. Get information about a text file");
        System.out.println("9. Quit");
        System.out.println();
    }

    /**
     * Prints the file extensions menu
     *
     * @param fileExtensions
     */
    public static void showFileExtensionsMenu(String[] fileExtensions) {
        System.out.println("The present extensions are:");
        System.out.println();
        printArrayNumbered(fileExtensions);
        System.out.println();
        System.out.println("9. Back");
        System.out.println();
        System.out.println("Choose the extension that you want to show a file list from.");
    }

    /**
     * Prints the text file manipulations menu
     */
    public static void showFileInfoMenu(String fileName) {
        System.out.println("What do you want to do with the file " + fileName + "?");
        System.out.println();
        System.out.println("1. Show the name of the file.");
        System.out.println("2. Show file size of " + fileName);
        System.out.println("3. Show number of lines in " + fileName);
        System.out.println("4. See if specified word is in " + fileName);
        System.out.println("5. See number of occurrences of specified word in " + fileName);
        System.out.println();
        System.out.println("9. Back");
    }

    /**
     * Print the size of a given file.
     * @param fileName
     * @param fileSize
     */
    public static void printFileSize(String fileName, long fileSize, long elapsedTime) {
        logAndWriteToOutput(fileName + " is " + fileSize / 1000 + "kB", elapsedTime);
    }

    /**
     * Print the number of lines of a given file.
     * @param fileName
     * @param numberOfLines
     */
    public static void printNumberOfLines(String fileName, long numberOfLines) {
        System.out.println(fileName + " has " + numberOfLines + " lines.");
    }

    /**
     * Prints the strings from the given array
     *
     * @param array
     */
    public static void printArray(String[] array) {
        for (String string : array) {
            System.out.println(string);
        }
    }

    /**
     * Like {@link printArray}, but this each item is prefixed with a menu item
     * number.
     *
     * @param array
     */
    public static void printArrayNumbered(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d. %s\n", i + 1, array[i]);
        }
    }

    public static void printExitMessage() {
        System.out.println("Alright, see ya!");
    }

    /**
     * Writes a log and prints the same message to the output.
     * @param message
     * @param elapsedTime
     */
    private static void logAndWriteToOutput(String message, long elapsedTime) {
        Logger.writeToLog(message, elapsedTime);
        System.out.println(message);
    }
}
