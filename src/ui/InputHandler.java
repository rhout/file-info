package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Handles user input
 */
public class InputHandler {
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private static final int BACK_OR_EXIT_CODE = 9;

    /**
     * Reads console input from a user until an integer is provided
     *
     * @param validRange The highest value the user can chose
     * @return The int entered by the user
     */
    public static int getIntFromUser(int validRange) {
        String input = "";
        int userChoice = -1;
        System.out.print("Please enter a value: ");
        try {
            boolean isValid;
            do {
                input = bufferedReader.readLine();
                userChoice = tryParseInt(input);
                isValid = isValidInt(userChoice, validRange);
                if (!isValid) {
                    System.out.println("Valid numbers are 1 -  " + validRange + " and 9.");
                }
            } while (!isValid);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return userChoice;
    }

    /**
     * Checks if a user input is a valid int and if the int is either in the
     * specified valid range or the back/exit number
     *
     * @param userChoice
     * @param maximumValue
     * @return
     */
    private static boolean isValidInt(int userChoice, int maximumValue) {
        return userChoice > 0 && (userChoice <= maximumValue || userChoice == BACK_OR_EXIT_CODE);
    }

    /**
     * Reads a string provided by the user
     *
     * @return The string entered by the user
     */
    public static String getWordFromUser() {
        System.out.println("Which word do you want to search for?");
        try {
            String input = bufferedReader.readLine();
            if (input == "") {
                System.out.println("You need to enter at least one character.");
                return getWordFromUser();
            }
            return input;
        } catch (IOException ex) {
            System.out.println("An error occurred trying to read your word.");
            System.out.println(ex.getMessage());
            return "";
        }
    }

    /**
     * Parses int from a string. If the string is not a representation of an int, -1
     * will be returned.
     *
     * @param stringToParse
     * @return The parsed int or -1 if the string is not a number
     */
    private static int tryParseInt(String stringToParse) {
        return tryParseInt(stringToParse, 0);
    }

    /**
     * Parses int from a string.
     *
     * @param stringToParse
     * @param defaultValue  The value to return if the string can't be parsed as an
     *                      int
     * @return The int parsed from the string
     */
    private static int tryParseInt(String stringToParse, int defaultValue) {
        try {
            int userInput = Integer.parseInt(stringToParse);
            if (userInput > 0) {
                return userInput;
            }
        } catch (NumberFormatException e) {
            System.out.println("Only numbers are accepted.");
            return defaultValue;
        }
        return defaultValue;
    }
}
