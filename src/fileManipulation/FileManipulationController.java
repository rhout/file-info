package fileManipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileManipulationController {
    private final String RESOURCE_FOLDER_NAME = "resources";

    /**
     * Gets an array of all the file names in the folder.
     *
     * @return
     */
    public String[] getFileNamesInFolder() {
        ProgramFile[] files = getFilesInFolder();
        String[] fileNames = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            fileNames[i] = files[i].getFullFileName();
        }
        return fileNames;
    }

    /**
     * Gets an array of the distinct file extensions in the folder.
     *
     * @return Contains the file extensions in the form ".(extension)".
     */
    public String[] getExistingFileExtensionsInFolder() {
        Set<String> fileNames = new HashSet<String>();
        ProgramFile[] fileExtensions = getFilesInFolder();
        for (ProgramFile file : fileExtensions) {
            fileNames.add(file.getExtensionName());
        }
        return fileNames.toArray(new String[0]);
    }

    /**
     * Creates an array containing all files with specified extension type.
     *
     * @param extensionType
     * @return
     */
    public String[] getFilesWithExtension(String extensionType) {
        ProgramFile[] filesInDirectory = getFilesInFolder();
        ProgramFile[] filesWithSpecifiedExtension = Arrays.stream(filesInDirectory)
                .filter(file -> file.getExtensionName().equals(extensionType)).toArray(ProgramFile[]::new);
        String[] fileNames = new String[filesWithSpecifiedExtension.length];
        for (int i = 0; i < fileNames.length; i++) {
            fileNames[i] = filesWithSpecifiedExtension[i].getFullFileName();
        }
        return fileNames;
    }

    /**
     * Creates a list with ProgramFile-objects of the files in the folder.
     *
     * @return Array containing the files.
     */
    public ProgramFile[] getFilesInFolder() {
        List<ProgramFile> fileNames = new ArrayList<ProgramFile>();
        try {
            final File directory = new File(RESOURCE_FOLDER_NAME);
            for (final File fileEntry : directory.listFiles()) {
                fileNames.add(new ProgramFile(fileEntry.getName()));
            }
        } catch (NullPointerException e) {
            System.out.println("You seem to have moved or deleted your resource folder...");
        }
        return fileNames.toArray(new ProgramFile[0]);
    }

    /**
     * Nice and redundant method since we already have the file name....
     *
     * @param name
     * @return The file name.
     */
    public String getFileNameOfFileBy(String name) {
        try {
            File file = getFileBy(name);
            return file.getName();
        } catch (NullPointerException e) {
            return "The file seem to have dissappeared...";
        }
    }

    /**
     * Gets the file size of specified file name
     *
     * @param fileName
     * @return The file name.
     */
    public long getFileSizeOfFile(String fileName) {
        try {
            return Files.size(Paths.get(RESOURCE_FOLDER_NAME + "/" + fileName));
        } catch (IOException | InvalidPathException e2) {
            return 0;
        }
    }

    /**
     * Get number of lines in a specified file.
     *
     * @param fileName
     * @return
     */
    public long getNumberOfLinesInFile(String fileName) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(getFileBy(fileName)))) {
            long numberOfLines = 0;
            while (bufferedReader.readLine() != null) {
                numberOfLines++;
            }
            return numberOfLines;
        } catch (FileNotFoundException e) {
            return 0;
        } catch (IOException e) {
            System.out.println("An error occured reading the line (or closing the buffer)");
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Checks if a specified word is present in specified file.
     *
     * @param fileName
     * @param word     to search for
     * @return
     */
    public boolean isWordPresentInFile(String fileName, String word) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(getFileBy(fileName)))) {
            String line;
            Pattern pattern = Pattern.compile("(?i)\\b" + word + "\\b");
            while ((line = bufferedReader.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    return true;
                }
            }
        } catch (IOException ex) {
            System.out.println("There was an error reading the line. Please try again.");
            System.out.println(ex.getMessage());
        }
        return false;
    }

    /**
     * Counts the occurrences of a specified word in the given text file
     *
     * @param fileName
     * @param wordToCount to count
     * @return
     */
    public long occurrencesOfWordInFile(String fileName, String wordToCount) {
        long occurrences = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(getFileBy(fileName)))) {
            String line;
            Pattern pattern = Pattern.compile("(?i)\\b" + wordToCount + "\\b");
            while ((line = bufferedReader.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                while (matcher.find()) {
                    occurrences ++;
                }
            }
        } catch (IOException ex) {
            System.out.println("There was an error reading the line. Please try again.");
            System.out.println(ex.getMessage());
        }
        return occurrences;
    }

    /**
     * Gets the File object from file name in the resource folder.
     *
     * @param fileName
     * @return
     * @throws NullPointerException
     */
    public File getFileBy(String fileName) throws NullPointerException {
        return new File(RESOURCE_FOLDER_NAME + "/" + fileName);
    }

    /**
     * Helper class to make it easier to separate file name and extension name.
     */
    private class ProgramFile {
        private String fileName;
        private String extenstion;

        public ProgramFile(String fullFileName) {
            int indexOfExtensionDelimiter = fullFileName.lastIndexOf(".");
            this.fileName = fullFileName.substring(0, indexOfExtensionDelimiter);
            this.extenstion = fullFileName.substring(indexOfExtensionDelimiter);
        }

        public String getExtensionName() {
            return extenstion;
        }

        public String getFullFileName() {
            return fileName + extenstion;
        }
    }
}
