# Welcome to my fancy file reader!
Show information about files in the resource folder. This folder has to be located in the root of the src-folder.

Things you can do:
1. List all the files in the resource folder
2. Chose a file extension and then list all files with that extension
3. Show information about a text-file from the resource folder:
   1. Show the name of the file
   2. Show the size of the file in kB
   3. Count the number of lines in the file
   4. See if a specified word exists in the file (case insensitive)
   5. Count the occurrences of a specified word in the file (case insensitive)

Have fun! :fire:
    
<br/>
<br/>

## Screenshots for assignment submission
### Compiling using javac:
![Compiling using javac](https://gitlab.com/rhout/file-info/-/raw/master/resources/javac.PNG "Generating class-files")

### Generating the jar file:
![Generating jar file](https://gitlab.com/rhout/file-info/-/raw/master/resources/jar.PNG "Command for generating the jar-file")

### Running the jar:
![Running the jar](https://gitlab.com/rhout/file-info/-/raw/master/resources/programRunning.PNG "A running instance of the program")